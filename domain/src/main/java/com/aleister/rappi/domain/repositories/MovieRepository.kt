package com.aleister.rappi.domain.repositories

import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.model.Video
import kotlinx.coroutines.flow.Flow

interface MovieRepository {

    fun getPopularMovieList(): Flow<Outcome<List<Movie>>>
    fun getTopRatedMovieList(): Flow<Outcome<List<Movie>>>
    fun getVideoList(movieId: String): Flow<Outcome<List<Video>>>

}