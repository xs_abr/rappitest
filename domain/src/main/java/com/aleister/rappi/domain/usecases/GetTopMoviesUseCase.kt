package com.aleister.rappi.domain.usecases

import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.repositories.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetTopMoviesUseCase @Inject constructor(private val repository: MovieRepository) :
    FlowUseCase<Outcome<List<Movie>>>() {

    override fun performAction(params: Map<String, Any>): Flow<Outcome<List<Movie>>> =
        repository.getPopularMovieList()

}