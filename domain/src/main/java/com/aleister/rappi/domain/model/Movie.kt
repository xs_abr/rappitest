package com.aleister.rappi.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val id: String,
    val name: String,
    val posterPath: String,
    val overview: String
) : Parcelable