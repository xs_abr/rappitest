package com.aleister.rappi.domain.usecases

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest

@ExperimentalCoroutinesApi
abstract class FlowUseCase<T> {

    /**
     * Trigger for the action which can be done in this request
     */
    private val _trigger = MutableStateFlow(emptyMap<String, Any>())

    /**
     * Exposes result of this use case
     */
    val resultFlow: Flow<T> = _trigger.flatMapLatest {
        performAction(it)
    }

    protected abstract fun performAction(params: Map<String, Any>): Flow<T>

    /**
     * Triggers the execution of this use case
     */
    suspend fun launch(params: Map<String, Any> = emptyMap()) {
        _trigger.emit(params)
    }
}
