package com.aleister.rappi.domain.usecases

abstract class SuspendUseCase<T> {

    abstract suspend fun create(data: Map<String, Any>? = null): T

    suspend fun execute(withData: Map<String, Any>? = null): T {
        return create(withData)
    }
}