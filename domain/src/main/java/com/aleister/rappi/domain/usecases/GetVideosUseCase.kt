package com.aleister.rappi.domain.usecases

import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Video
import com.aleister.rappi.domain.repositories.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetVideosUseCase @Inject constructor(val movieRepository: MovieRepository) :
    FlowUseCase<Outcome<List<Video>>>() {

    override fun performAction(params: Map<String, Any>): Flow<Outcome<List<Video>>> {
        val movieId = if (params.containsKey("id")) params["id"] as String else ""
        return movieRepository.getVideoList(movieId)
    }


}