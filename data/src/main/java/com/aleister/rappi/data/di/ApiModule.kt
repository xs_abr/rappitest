package com.aleister.rappi.data.di

import com.aleister.rappi.data.api.Api
import com.aleister.rappi.data.api.AppApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    @Singleton
    fun provideApi(): Api = AppApi()

}