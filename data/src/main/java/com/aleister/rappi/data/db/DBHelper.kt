package com.aleister.rappi.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.aleister.rappi.data.db.dao.MovieDao
import com.aleister.rappi.data.db.model.LocalMovie

@Database(entities = [LocalMovie::class], version = 1, exportSchema = true)
abstract class DBHelper : RoomDatabase() {

    abstract fun movieDao(): MovieDao

}