package com.aleister.rappi.data.api.response

import com.google.gson.annotations.SerializedName

class GetVideoListResponse(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("results") var results: List<RemoteVideo>? = null
)