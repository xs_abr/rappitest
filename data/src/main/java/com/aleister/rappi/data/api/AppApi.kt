package com.aleister.rappi.data.api

import com.aleister.rappi.data.api.response.GetMovieListResponse
import com.aleister.rappi.data.api.response.GetVideoListResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppApi
@Inject constructor() : Api {

    private val apiHelper: Api by lazy { getClient().create(Api::class.java) as Api }
    private val gson: Gson by lazy { GsonBuilder().setLenient().create() }

    private fun getClient(): Retrofit {
        val client = OkHttpClient.Builder()
        client.addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .retryOnConnectionFailure(true)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)

        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client.build())
            .build()
    }

    override suspend fun getPopularMoviesAsync(
        apiKey: String
    ): GetMovieListResponse = apiHelper.getPopularMoviesAsync(apiKey)

    override suspend fun getTopRatedMoviesAsync(apiKey: String): GetMovieListResponse =
        apiHelper.getTopRatedMoviesAsync(apiKey)

    override suspend fun getVideosAsync(movieId: String, apiKey: String): GetVideoListResponse =
        apiHelper.getVideosAsync(movieId, apiKey)
}