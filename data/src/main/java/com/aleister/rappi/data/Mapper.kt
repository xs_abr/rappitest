package com.aleister.rappi.data

import com.aleister.rappi.data.api.response.GetMovieListResponse
import com.aleister.rappi.data.api.response.GetVideoListResponse
import com.aleister.rappi.data.db.model.LocalMovie
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.model.Video

fun GetMovieListResponse.toLocalMovie(): List<LocalMovie> {
    return results?.map {
        LocalMovie(it.id, it.original_title ?: "", it.poster_path ?: "", it.overview ?: "")
    } ?: listOf()
}

fun GetVideoListResponse.toVideos(): List<Video> {
    return results?.map {
        Video("https://www.youtube.com/watch?v=" + it.key)
    } ?: listOf()
}

fun List<LocalMovie>.toMovieList(): List<Movie> {
    return this.map {
        Movie(it.id, it.name, it.posterPath, it.overview)
    }
}