package com.aleister.rappi.data.api

import com.aleister.rappi.data.api.response.GetMovieListResponse
import com.aleister.rappi.data.api.response.GetVideoListResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    /*@POST("user/register")
    fun postUserRegisterAsync(
        @Body registerRequestModel: RegisterRequestModel
    ): Deferred<Response<RegisterResponseModel>>*/

    @GET("movie/popular")
    suspend fun getPopularMoviesAsync(@Query("api_key") apiKey: String): GetMovieListResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedMoviesAsync(@Query("api_key") apiKey: String): GetMovieListResponse

    @GET("movie/{movie_id}/videos")
    suspend fun getVideosAsync(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String,
    ): GetVideoListResponse
}