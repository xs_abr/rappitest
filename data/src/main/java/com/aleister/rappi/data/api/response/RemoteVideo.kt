package com.aleister.rappi.data.api.response

class RemoteVideo(
    val site: String? = null,
    val key: String? = null
)