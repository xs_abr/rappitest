package com.aleister.rappi.data.repository

import android.util.Log
import com.aleister.rappi.data.api.Api
import com.aleister.rappi.data.db.DBHelper
import com.aleister.rappi.data.toLocalMovie
import com.aleister.rappi.data.toMovieList
import com.aleister.rappi.data.toVideos
import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.model.Video
import com.aleister.rappi.domain.repositories.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class MovieRepositoryImpl(
    private val api: Api,
    private val dbHelper: DBHelper
) : MovieRepository {

    override fun getPopularMovieList(): Flow<Outcome<List<Movie>>> = flow {
        emit(Outcome.loading(partialData = dbHelper.movieDao().getMovies().toMovieList()))
        val response = api.getPopularMoviesAsync("b684fdb0e1bca2ef81acdb4d16b5f2fd")
        dbHelper.movieDao().insert(response.toLocalMovie() ?: listOf())
        emit(Outcome.success(dbHelper.movieDao().getMovies().toMovieList()))
    }.catch {
        Log.e("MovieRepository", "Error", it)
        emit(Outcome.failure(it))
    }.flowOn(Dispatchers.IO)

    override fun getTopRatedMovieList(): Flow<Outcome<List<Movie>>> = flow {
        emit(Outcome.loading(partialData = dbHelper.movieDao().getMovies().toMovieList()))
        val response = api.getTopRatedMoviesAsync("b684fdb0e1bca2ef81acdb4d16b5f2fd")
        dbHelper.movieDao().insert(response.toLocalMovie() ?: listOf())
        emit(Outcome.success(dbHelper.movieDao().getMovies().toMovieList()))
    }.catch {
        Log.e("MovieRepository", "Error", it)
        emit(Outcome.failure(it))
    }.flowOn(Dispatchers.IO)

    override fun getVideoList(movieId: String): Flow<Outcome<List<Video>>> = flow {
        emit(Outcome.loading())
        val response = api.getVideosAsync(movieId, "b684fdb0e1bca2ef81acdb4d16b5f2fd")
        val videoList = response.toVideos()
        emit(Outcome.success(videoList))
    }.catch {
        Log.e("MovieRepository", "Error", it)
        emit(Outcome.failure(it))
    }.flowOn(Dispatchers.IO)

}