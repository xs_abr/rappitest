package com.aleister.rappi.data.di

import com.aleister.rappi.data.api.Api
import com.aleister.rappi.data.db.DBHelper
import com.aleister.rappi.data.repository.MovieRepositoryImpl
import com.aleister.rappi.domain.repositories.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    fun provideMovieRepository(
        api: Api,
        dbHelper: DBHelper
    ): MovieRepository = MovieRepositoryImpl(api, dbHelper)

}