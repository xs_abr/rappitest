package com.aleister.rappi.data.di

import android.content.Context
import androidx.room.Room
import com.aleister.rappi.data.db.DBHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DBModule {

    @Provides
    @Singleton
    fun provideDBHelper(@ApplicationContext appContext: Context): DBHelper =
        Room.databaseBuilder(appContext, DBHelper::class.java, "rappi").build()

}