package com.aleister.rappi.data.di

import com.aleister.rappi.domain.repositories.MovieRepository
import com.aleister.rappi.domain.usecases.GetTopMoviesUseCase
import com.aleister.rappi.domain.usecases.GetVideosUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    fun provideGetTopMovies(movieRepository: MovieRepository): GetTopMoviesUseCase =
        GetTopMoviesUseCase(movieRepository)

    @Provides
    fun provideGetVideosUseCase(movieRepository: MovieRepository): GetVideosUseCase =
        GetVideosUseCase(movieRepository)
}