package com.aleister.rappi.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Movies")
class LocalMovie(
    @PrimaryKey val id: String,
    val name: String,
    val posterPath: String,
    val overview: String
) {
}