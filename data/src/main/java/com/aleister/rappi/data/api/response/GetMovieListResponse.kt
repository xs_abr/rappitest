package com.aleister.rappi.data.api.response

import com.google.gson.annotations.SerializedName

class GetMovieListResponse(
    @SerializedName("page") var page: Int? = null,
    @SerializedName("total_results") var totalResults: Int? = null,
    @SerializedName("total_pages") var totalPages: Int? = null,
    @SerializedName("results") var results: List<RemoteMovie>? = null
)