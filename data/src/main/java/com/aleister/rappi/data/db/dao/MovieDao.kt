package com.aleister.rappi.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.aleister.rappi.data.db.model.LocalMovie

@Dao
abstract class MovieDao : BaseDao<LocalMovie> {
    @Query("SELECT * FROM 'Movies'")
    abstract fun getMovies(): List<LocalMovie>
}