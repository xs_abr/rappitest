# Test de Rappi

Desarrollado por Alejandro Bautista

## Detalles
    -Clean Architecture
    -MVVM
    -Room
    -Retrofit
    -View Binding

## Mejoras
    -Se tiene q replicar los fragmentos para TopRated Movies y Series
    -Para la busqueda se crea una consulta a la tabla y con un LiveData se esperan los resultados
    -Falta la reproduccion de videos ya no me dio tiempo de checar que fallo



