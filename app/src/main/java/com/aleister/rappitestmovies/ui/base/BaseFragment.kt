package com.aleister.rappitestmovies.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.aleister.rappitestmovies.ui.common.LoadingDialog

abstract class BaseFragment<Binding : ViewBinding, ViewModel : BaseViewModel> : Fragment() {

    lateinit var navController: NavController
    protected abstract val viewModel: ViewModel
    protected lateinit var binding: Binding
    private val loading = LoadingDialog.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navController = findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getViewBinding(inflater, container)
        setupBaseView()
        setupView()
        return binding.root
    }

    private fun setupBaseView() {
        viewModel.onLoading().observe(viewLifecycleOwner, {
            if (it)
                loading?.show(requireActivity(), "Loading...", false)
            else
                loading?.hide()
        })
        viewModel.onError().observe(viewLifecycleOwner, {
            onError(it)
        })
    }

    fun onError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    abstract fun getViewBinding(inflater: LayoutInflater, container: ViewGroup?): Binding

    abstract fun setupView()


}