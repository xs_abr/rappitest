package com.aleister.rappitestmovies.ui.home.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappitestmovies.R
import com.aleister.rappitestmovies.utils.loadImage

class MoviesAdapter(private val onMovieSelected: (movie: Movie) -> Unit) :
    ListAdapter<Movie, MovieViewHolder>(DiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = getItem(position)
        holder.apply {
            tvTitle.text = movie.name
            ivPhoto.loadImage(movie.posterPath)
            tvOverView.text = movie.overview
            rootView.setOnClickListener { onMovieSelected(movie) }
        }
    }
}

class DiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem.name == newItem.name
}