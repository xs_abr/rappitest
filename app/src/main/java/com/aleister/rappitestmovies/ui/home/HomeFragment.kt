package com.aleister.rappitestmovies.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappitestmovies.R
import com.aleister.rappitestmovies.databinding.FragmentHomeBinding
import com.aleister.rappitestmovies.ui.base.BaseFragment
import com.aleister.rappitestmovies.ui.home.adapter.MoviesAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    override val viewModel: HomeViewModel by viewModels()
    private val moviesAdapter = MoviesAdapter { onMovieSelected(it) }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false)

    override fun setupView() {
        binding.apply {
            rvMovies.adapter = moviesAdapter
        }
        viewModel.apply {
            moviesLiveData.observe(this@HomeFragment, {
                submit(it)
            })
        }
        viewModel.start()
    }

    private fun submit(outcome: Outcome<List<Movie>>?) {
        when (outcome) {
            is Outcome.Progress -> {
                binding.srlMovies.isRefreshing = true
                moviesAdapter.submitList(outcome.partialData)
            }
            is Outcome.Failure -> {
                binding.srlMovies.isRefreshing = false
                onError(outcome.e.message ?: "Error")
            }
            is Outcome.Success -> {
                binding.srlMovies.isRefreshing = false
                moviesAdapter.submitList(outcome.data)
            }
        }
    }

    private fun onMovieSelected(movie: Movie) {
        findNavController().navigate(
            R.id.action_navigation_home_to_navigation_details,
            Bundle().apply {
                putParcelable("movie", movie)
            })
    }

}