package com.aleister.rappitestmovies.ui.home.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.aleister.rappitestmovies.R

class MovieViewHolder(private val itemView: View) : ViewHolder(itemView) {
    val rootView = itemView
    val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
    val ivPhoto = itemView.findViewById<ImageView>(R.id.ivPhoto)
    val tvOverView = itemView.findViewById<TextView>(R.id.tvOverview)
}