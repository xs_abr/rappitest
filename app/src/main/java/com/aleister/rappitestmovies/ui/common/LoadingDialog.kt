package com.aleister.rappitestmovies.ui.common

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.widget.TextView
import com.aleister.rappitestmovies.R

class LoadingDialog {
    private var mDialog: Dialog? = null

    fun show(context: Context, message: String = "", cancelable: Boolean) {
        if (mDialog != null) {
            mDialog!!.dismiss()
        }
        mDialog = Dialog(context)
        // no tile for the dialog
        mDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog!!.setContentView(R.layout.loading_dialog)
        val loadingText = mDialog!!.findViewById<TextView>(R.id.loadingText)
        if (message.isNotEmpty()) {
            loadingText.text = message
        } else {
            loadingText.visibility = View.GONE
        }
        mDialog!!.setCancelable(cancelable)
        mDialog!!.setCanceledOnTouchOutside(cancelable)
        mDialog!!.show()
    }

    fun hide() {
        if (mDialog != null) {
            mDialog!!.dismiss()
            mDialog = null
        }
    }

    companion object {
        private var loadingDialog: LoadingDialog? = null
        fun getInstance(): LoadingDialog? {
            return getInstance(true)
        }

        fun getInstance(singleton: Boolean): LoadingDialog? {
            return if (singleton) {
                if (singleton && loadingDialog == null) {
                    loadingDialog = LoadingDialog()
                }
                loadingDialog
            } else {
                LoadingDialog()
            }
        }

    }
}