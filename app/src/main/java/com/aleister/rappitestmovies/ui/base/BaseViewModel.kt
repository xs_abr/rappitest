package com.aleister.rappitestmovies.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    protected val loadingMLD = MutableLiveData<Boolean>()
    protected val errorMLD = MutableLiveData<String>()


    fun onLoading(): LiveData<Boolean> = loadingMLD
    fun onError(): LiveData<String> = errorMLD
}