package com.aleister.rappitestmovies.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.model.Video
import com.aleister.rappi.domain.usecases.GetVideosUseCase
import com.aleister.rappitestmovies.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(val getVideosUseCase: GetVideosUseCase) :
    BaseViewModel() {

    val videosLiveData: LiveData<Outcome<List<Video>>> = getVideosUseCase.resultFlow.asLiveData()

    fun star(movie: Movie) {
        viewModelScope.launch {
            getVideosUseCase.launch(mapOf("id" to movie.id))
        }
    }


}