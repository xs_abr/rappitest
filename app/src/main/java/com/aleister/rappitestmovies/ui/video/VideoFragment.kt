package com.aleister.rappitestmovies.ui.video

import android.net.Uri
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.aleister.rappitestmovies.databinding.FragmentVideoBinding
import com.aleister.rappitestmovies.ui.base.BaseFragment
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource


class VideoFragment : BaseFragment<FragmentVideoBinding, VideoViewModel>() {
    override val viewModel: VideoViewModel by viewModels()
    private lateinit var videoPlayer: SimpleExoPlayer
    var videoUrl = ""

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentVideoBinding = FragmentVideoBinding.inflate(inflater, container, false)

    override fun setupView() {
        initializePlayer()
    }

    private fun buildMediaSource(videoUrl: String): MediaSource? {
        val dataSourceFactory = DefaultDataSourceFactory(requireContext(), "sample")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(videoUrl))
    }

    private fun initializePlayer() {
        videoPlayer = SimpleExoPlayer.Builder(requireContext()).build()
        binding.videoPlayerView.player = videoPlayer
        playYoutubeVideo(videoUrl)
    }

    private fun playYoutubeVideo(videoUrl: String) {
        val videoExtractor = object : YouTubeExtractor(requireContext()) {
            override fun onExtractionComplete(ytFiles: SparseArray<YtFile>?, vMeta: VideoMeta?) {
                if (ytFiles != null) {
                    val videoTag = 137
                    val audioTag = 140
                    val audioSource =
                        ProgressiveMediaSource.Factory(DefaultHttpDataSource.Factory())
                            .createMediaSource(
                                MediaItem.fromUri(ytFiles.get(audioTag).url)
                            )
                    val videoSource =
                        ProgressiveMediaSource.Factory(DefaultHttpDataSource.Factory())
                            .createMediaSource(
                                MediaItem.fromUri(ytFiles.get(videoTag).url)
                            )
                    videoPlayer.setMediaSource(
                        MergingMediaSource(
                            true,
                            videoSource,
                            audioSource
                        ), true
                    )
                    videoPlayer.prepare()
                    videoPlayer.playWhenReady = true
                }
            }
        }.extract(videoUrl, false, true)
    }
}