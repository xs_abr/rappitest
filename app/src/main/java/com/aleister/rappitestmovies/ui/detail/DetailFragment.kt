package com.aleister.rappitestmovies.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.model.Video
import com.aleister.rappitestmovies.databinding.FragmentDetailBinding
import com.aleister.rappitestmovies.ui.base.BaseFragment
import com.aleister.rappitestmovies.ui.video.VideoFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding, DetailViewModel>() {
    override val viewModel: DetailViewModel by viewModels()
    private lateinit var videoPagerAdapter: VideosPagerAdapter

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDetailBinding = FragmentDetailBinding.inflate(inflater, container, false)

    override fun setupView() {
        videoPagerAdapter = VideosPagerAdapter(this)
        binding.vpVideos.adapter = videoPagerAdapter
        val movie = arguments?.getParcelable<Movie>("movie")
        movie?.let {
            binding.tvTitle.text = it.name
            binding.tvOverview.text = it.overview
            viewModel.star(it)
        }
        viewModel.videosLiveData.observe(this, {
            onVideosUpdated(it)
        })
    }

    private fun onVideosUpdated(result: Outcome<List<Video>>) {
        when (result) {
            is Outcome.Progress -> {

            }
            is Outcome.Failure -> {

            }
            is Outcome.Success -> {
                videoPagerAdapter.updateAdapter(result.data)
            }
        }
    }


    private inner class VideosPagerAdapter(fragment: Fragment) :
        FragmentStateAdapter(fragment) {

        var videoList: List<Video> = listOf()

        override fun getItemCount(): Int = videoList.size

        override fun createFragment(position: Int): Fragment = VideoFragment().apply {
            videoUrl = videoList[position].url
        }

        fun updateAdapter(videoList: List<Video>) {
            this.videoList = videoList
            notifyDataSetChanged()
        }
    }

}