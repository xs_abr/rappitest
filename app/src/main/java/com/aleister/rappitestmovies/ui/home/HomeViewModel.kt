package com.aleister.rappitestmovies.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.aleister.rappi.domain.common.Outcome
import com.aleister.rappi.domain.model.Movie
import com.aleister.rappi.domain.usecases.GetTopMoviesUseCase
import com.aleister.rappitestmovies.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val getTopMoviesUseCase: GetTopMoviesUseCase) :
    BaseViewModel() {

    val moviesLiveData: LiveData<Outcome<List<Movie>>> = getTopMoviesUseCase.resultFlow.asLiveData()

    fun start() {
        viewModelScope.launch {
            //getTopMoviesUseCase.launch()
        }
    }

}