package com.aleister.rappitestmovies.utils

import android.widget.ImageView
import com.aleister.rappitestmovies.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadImage(imgUrl: String?) {
    Glide.with(this.context)
        .load("https://image.tmdb.org/t/p/w300_and_h300_bestv2" + imgUrl)
        .apply(
            RequestOptions()
                .placeholder(R.drawable.loading_animation)
                .circleCrop()
        ).into(this)
}